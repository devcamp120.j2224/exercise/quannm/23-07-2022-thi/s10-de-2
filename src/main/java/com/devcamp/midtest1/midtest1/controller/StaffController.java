package com.devcamp.midtest1.midtest1.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.midtest1.midtest1.model.Staff;
import com.devcamp.midtest1.midtest1.service.StaffService;

@RestController
public class StaffController {
    @CrossOrigin
    @GetMapping("/staffs")
    public ArrayList<Staff> getAllStaff() {
        StaffService listStaff = new StaffService();
        return listStaff.getListAllStaff();
    }

    @CrossOrigin
    @GetMapping("/staffsAge")
    public ArrayList<Staff> getStaffFromAge(@RequestParam(required = true, name = "age") int age) {
        StaffService listStaff = new StaffService();
        ArrayList<Staff> listFound = new ArrayList<>();
        for (Staff staff : listStaff.getListAllStaff()) {
            if (staff.getAge() > age) {
                listFound.add(staff);
            }
        }
        return listFound;
    }
}
