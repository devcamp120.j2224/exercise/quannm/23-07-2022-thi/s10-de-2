package com.devcamp.midtest1.midtest1.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.midtest1.midtest1.model.Department;
import com.devcamp.midtest1.midtest1.service.DepartmentService;

@RestController
public class DepartmentController {
    @CrossOrigin
    @GetMapping("/departments")
    public ArrayList<Department> getAllDepartment() {
        DepartmentService listDep = new DepartmentService();
        return listDep.getListDepartment();
    }
    
    @CrossOrigin
    @GetMapping("/department-info")
    public Department getDepartmentFromId(@RequestParam(required = true, name = "departmentId") int id) {
        DepartmentService listDep = new DepartmentService();
        Department departmentFound = new Department();
        for (Department department : listDep.getListDepartment()) {
            if (department.getId() == id) {
                departmentFound = department;
            }
        }
        return departmentFound;
    }

    @CrossOrigin
    @GetMapping("/departmentInfoFromAverageAge")
    public ArrayList<Department> getDepartmentFromAverageAge(@RequestParam(required = true, name = "averageAge") int age) {
        DepartmentService listDep = new DepartmentService();
        ArrayList<Department> listFound = new ArrayList<>();
        for (Department department : listDep.getListDepartment()) {
            if (department.getAverageAge() > age) {
                listFound.add(department);
            }
        }
        return listFound;
    }
}
