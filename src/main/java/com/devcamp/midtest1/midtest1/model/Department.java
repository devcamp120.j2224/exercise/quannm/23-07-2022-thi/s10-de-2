package com.devcamp.midtest1.midtest1.model;

import java.util.ArrayList;

public class Department {
    private int id;
    private String name;
    private String address;
    private ArrayList<Staff> staffs;

    public Department() {
    }

    public Department(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Department(int id, String name, String address, ArrayList<Staff> staffs) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.staffs = staffs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<Staff> getStaffs() {
        return staffs;
    }

    public void setStaffs(ArrayList<Staff> staffs) {
        this.staffs = staffs;
    }
    
    public float getAverageAge() {
        float averageAge;
        int totalAge = 0;
        for (int i = 0; i < getStaffs().size(); i++) {
            totalAge += getStaffs().get(i).getAge();
        }
        averageAge = totalAge / getStaffs().size();
        return averageAge;
        
    }
}
