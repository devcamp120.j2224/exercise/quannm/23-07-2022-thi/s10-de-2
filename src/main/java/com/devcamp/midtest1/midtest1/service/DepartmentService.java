package com.devcamp.midtest1.midtest1.service;

import java.util.ArrayList;

import com.devcamp.midtest1.midtest1.model.Department;

public class DepartmentService {
    public ArrayList<Department> getListDepartment() {
        ArrayList<Department> listDepartment = new ArrayList<>();
        StaffService listStaff = new StaffService();
        Department department1 = new Department(1, "Sky", "Q8, HCM", listStaff.getListStaff1());
        Department department2 = new Department(2, "Lotus", "Q6, HCM", listStaff.getListStaff2());
        Department department3 = new Department(3, "Garden", "Q.BT, HCM", listStaff.getListStaff3());
        listDepartment.add(department1);
        listDepartment.add(department2);
        listDepartment.add(department3);
        return listDepartment;
    }
}
