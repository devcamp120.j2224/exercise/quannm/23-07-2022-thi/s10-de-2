package com.devcamp.midtest1.midtest1.service;

import java.util.ArrayList;

import com.devcamp.midtest1.midtest1.model.Staff;

public class StaffService {
    public ArrayList<Staff> getListStaff1() {
        ArrayList<Staff> listStaff1 = new ArrayList<>();
        Staff staff1 = new Staff(1, "John", 30);
        Staff staff2 = new Staff(2, "Mark", 35);
        Staff staff3 = new Staff(3, "Mary", 28);
        listStaff1.add(staff1);
        listStaff1.add(staff2);
        listStaff1.add(staff3);
        return listStaff1;
    }

    public ArrayList<Staff> getListStaff2() {
        ArrayList<Staff> listStaff2 = new ArrayList<>();
        Staff staff4 = new Staff(4, "David", 32);
        Staff staff5 = new Staff(5, "Cait", 25);
        Staff staff6 = new Staff(6, "Cris", 33);
        listStaff2.add(staff4);
        listStaff2.add(staff5);
        listStaff2.add(staff6);
        return listStaff2;
    }

    public ArrayList<Staff> getListStaff3() {
        ArrayList<Staff> listStaff3 = new ArrayList<>();
        Staff staff7 = new Staff(7, "Rose", 26);
        Staff staff8 = new Staff(8, "Lucy", 29);
        Staff staff9 = new Staff(9, "Leo", 32);
        listStaff3.add(staff7);
        listStaff3.add(staff8);
        listStaff3.add(staff9);
        return listStaff3;
    }

    public ArrayList<Staff> getListAllStaff() {
        ArrayList<Staff> listAllStaff = new ArrayList<>();
        listAllStaff.addAll(getListStaff1());
        listAllStaff.addAll(getListStaff2());
        listAllStaff.addAll(getListStaff3());
        return listAllStaff;
    }
}
