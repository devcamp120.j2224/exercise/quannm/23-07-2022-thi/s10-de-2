package com.devcamp.midtest1.midtest1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Midtest1Application {

	public static void main(String[] args) {
		SpringApplication.run(Midtest1Application.class, args);
	}

}
